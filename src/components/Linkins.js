import React, { Component } from 'react'
import { Text, View, Button, Linking } from 'react-native'

export default class Linkins extends Component {

    openLocation() {
        const position = '-43.250511,-65.307345'
        const scheme = Platform.OS === 'ios' ? 'maps:' : 'geo:';
        const url = `${scheme}${position}`

        Linking.openURL(url);
    }

    openMail() {
        const url = `mailto:pabloatm980@gmail.com`
        Linking.openURL(url);
    }

    openLink() {
        const url = `http://www.dit.ing.unp.edu.ar/`
        Linking.openURL(url);
    }

    render() {
        return (
            <View style={{flex:1 , justifyContent:'space-evenly'}}>
                <Button title={'Mandar mail'} color="tomato" onPress={this.openMail}/>
                <Button title={'Abrir Location'} color="cornflowerblue" onPress={this.openLocation}/>
                <Button title={'Abrir link'} color="lightcoral" onPress={this.openLink}/>
            </View>
        )
    }
}
