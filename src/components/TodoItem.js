import React, { Component } from 'react'
import { Text, View, StyleSheet } from 'react-native'


const styles = StyleSheet.create({
    container: {
        marginVertical: 2,
        flex: 1,
        justifyContent: 'space-between',
        flexDirection: 'row',
        borderWidth:2,
        backgroundColor:'gainsboro',
        padding:3
        
    }
})
export default class TodoItem extends Component {
    render() {
        const { todo } = this.props
        return (
            <View style={styles.container}>
                <Text style={{maxWidth:300}}> {todo.title}</Text>
                <Text>{todo.completed ? "Hecho" : "Sin Hacer"} </Text>
            </View>

        )
    }
}
