import React, { Component } from 'react'
import { Text, View, FlatList } from 'react-native'

import TodoItem from './TodoItem'

export default class TodoList extends Component {
    render() {
        return (
            <FlatList
                data={this.props.todos}
                renderItem={({item}) => (<TodoItem todo={item}/>)}
                keyExtractor={item => item.id}
            />
        )
    }
}
